# Bank Account Web Service

This is a simple bank web service for simple bank balance, deposit and withdrawal actions

## How to build
It is a maven-based java application leveraging on Springboot. Embedded HSQL database is used as a relational datastore. To reset the data, simply restart the application.

```sh
mvn clean install 
```

The above will fetch maven dependencies and run unit tests before creating a runnable uber jar


## How to run

You can run the application as regular java jar application.

```sh
java -jar target/bank-account-service-0.0.1-SNAPSHOT.jar
```

The log output is available in the console or by accessing

```
target/logs/bank-service.log
```

## Accessing and testing application
The application exposes REST API endpoints which are easily testable using a Swagger interface

Go to http://localhost:8080/bank/swagger-ui.html

Use sample account numbers **1001** and **1002** to test the endpoints.


## Code coverage report

Java Code Coverage maven plugin is used to check for code coverage metrics upon running the unit tests. Run

```
mvn clean install jacoco:report
```
and access the report on your browser from 

```
target/site/jacoco/index.html
```