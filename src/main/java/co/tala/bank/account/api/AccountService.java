package co.tala.bank.account.api;

import co.tala.bank.account.api.models.AccountRequest;
import co.tala.bank.account.api.models.ApiResponse;
import co.tala.bank.account.entity.AccountActivity;
import co.tala.bank.account.entity.ActivityType;
import co.tala.bank.account.service.BankService;
import co.tala.bank.account.service.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Endpoints for Bank Account Service.
 *
 * @author kenny
 */
@RestController
@RequestMapping(path = "/api/account", produces = {MediaType.APPLICATION_JSON_VALUE})
public class AccountService {

    private final Logger log = LoggerFactory.getLogger(AccountService.class);
    @Autowired
    private BankService bankService;

    @RequestMapping(method = RequestMethod.GET, path = "/balance/{accountNumber}")
    public ResponseEntity<ApiResponse<? extends Object>> getAccountBalance(
            @PathVariable("accountNumber") String accountNumber) {
        log.info("GET /api/account/{}", accountNumber);
        ResponseEntity response;
        try {
            AccountActivity activityResult = bankService.transact(new AccountRequest(accountNumber), 
                    ActivityType.BALANCE_ENQUIRY);
            response = ResponseEntity.ok(new ApiResponse<>("00", activityResult));
        } catch (TransactionException ex) {
            log.error("Failed to execute Balance Enquiry for {}. {}", accountNumber, ex.getMessage());
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse<>("01", ex.getMessage()));
        }

        return response;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/deposit",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ApiResponse<? extends Object>> depositFunds(@RequestBody AccountRequest request) {
        log.info("POST /api/deposit payload={}", request);
        ResponseEntity response;
        try {
            AccountActivity activityResult = bankService.transact(request, ActivityType.FUNDS_DEPOSIT);
            response = ResponseEntity.ok(new ApiResponse<>("00", activityResult));
        } catch (TransactionException ex) {
            log.error("Failed to execute Funds Deposit for {}. {}", request, ex.getMessage());
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse<>("01", ex.getMessage()));
        }

        return response;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/withdraw",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ApiResponse<? extends Object>> withdrawFunds(@RequestBody AccountRequest request) {
        log.info("POST /api/withdraw payload={}", request);
        ResponseEntity response;
        try {
            AccountActivity activityResult = bankService.transact(request, ActivityType.FUNDS_WITHDRAWAL);
            response = ResponseEntity.ok(new ApiResponse<>("00", activityResult));
        } catch (TransactionException ex) {
            log.error("Failed to execute Funds Withdrawal for {}. {}", request, ex.getMessage());
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse<>("01", ex.getMessage()));
        }

        return response;
    }
}
