package co.tala.bank.account.api.models;

/**
 * Common response wrapper.
 *
 *
 * @author kenny
 * @param <T> Actual return-type.
 */
public class ApiResponse<T> {

    private String responseCode;
    private T entity;

    public ApiResponse() {
    }

    public ApiResponse(String responseCode, T entity) {
        this.responseCode = responseCode;
        this.entity = entity;
    }

    /**
     *
     * @return Wrapped entity or message.
     */
    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    /**
     *
     * @return '00' for success, any other is failure.
     */
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

}
