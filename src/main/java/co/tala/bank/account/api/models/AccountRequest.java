package co.tala.bank.account.api.models;

/**
 * Common payload for transaction request.
 *
 * @author kenny
 */
public class AccountRequest {

    private String accountNumber;
    private double amount = 0.0;

    public AccountRequest() {
    }

    public AccountRequest(String accountNumber, double amount) {
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    public AccountRequest(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.format("AccountRequest[accountNumber=%s,amount=%s]",
                accountNumber, amount);
    }

}
