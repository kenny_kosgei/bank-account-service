package co.tala.bank.account.repository;

import co.tala.bank.account.entity.Account;
import co.tala.bank.account.entity.AccountActivity;
import co.tala.bank.account.entity.ActivityType;
import co.tala.bank.account.entity.TransactionStatus;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Provides CRUD services for entity {@link AccountActivity}
 *
 * @author kenny
 */
@Repository
public interface AccountActivityRepository extends CrudRepository<AccountActivity, Long> {

    /**
     * Binds supplied parameters to the query used to fetch account activity for a given account in
     * a specified period.
     *
     * @param account target account.
     * @param activityType specific activity
     * @param transactionStatus select transactions status.
     * @param startDate begin date
     * @param endDate end date
     * @return matching results list or empty.
     */
    @Query(value = "SELECT a FROM AccountActivity a WHERE a.account=:account "
            + "AND a.transactionStatus=:transactionStatus "
            + "AND a.activityType=:activityType "
            + "AND a.dateCreated >= :startDate "
            + "AND a.dateCreated <= :endDate ORDER BY a.dateCreated")
    public List<AccountActivity> searchActivity(Account account, ActivityType activityType, TransactionStatus transactionStatus, Timestamp startDate, Timestamp endDate);
}
