package co.tala.bank.account.repository;

import co.tala.bank.account.entity.Account;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Provides CRUD services for entity {@link Account}
 *
 * @author kenny
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

    /**
     * Search an Account by account number.
     *
     * @param accountNumber specific account number.
     * @return Matching account or empty optional instance.
     */
    public Optional<Account> findDistinctByAccountNumber(String accountNumber);
}
