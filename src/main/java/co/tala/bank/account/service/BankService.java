package co.tala.bank.account.service;

import co.tala.bank.account.repository.AccountActivityRepository;
import co.tala.bank.account.api.models.AccountRequest;
import co.tala.bank.account.entity.Account;
import co.tala.bank.account.entity.AccountActivity;
import co.tala.bank.account.entity.ActivityType;
import co.tala.bank.account.entity.TransactionStatus;
import co.tala.bank.account.repository.AccountRepository;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to perform bank transactions against an account given an activity type.
 *
 * @author kenny
 */
@Component
@Transactional
public class BankService {

    private final Logger log = LoggerFactory.getLogger(BankService.class);
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountActivityRepository activityRepository;
    @Autowired
    private EntityManagerFactory emf;
    @Autowired
    private Environment environment;

    /**
     * Initializes service with preset accounts.
     */
    @PostConstruct
    protected void init() {
        log.info("Initializing bank service...");
        accountRepository.save(new Account("John Conor", "1001",
                new Timestamp(System.currentTimeMillis())));
        accountRepository.save(new Account("Sarag Conor", "1002",
                new Timestamp(System.currentTimeMillis())));
        log.info("Bank service initialized!");
    }

    /**
     * Handles bank account requests for various activity types.
     *
     * @param request account request input
     * @param activityType type of action to perform
     * @return account activity result.
     * @throws TransactionException erroneous conditions such as bad input, invalid operation etc.
     */
    public AccountActivity transact(AccountRequest request, ActivityType activityType) throws TransactionException {
        log.info("Performing {} upon {}", activityType, request);
        try {
            Optional<Account> account = accountRepository.findDistinctByAccountNumber(request.getAccountNumber());
            if (account.isPresent()) {
                AccountActivity activity = new AccountActivity(request.getAmount(), activityType, account.get());
                log.info("Begin {} for account {}...", activity, account.get());
                switch (activityType) {
                    case FUNDS_DEPOSIT: {
                        AccountActivity accountActivity = new AccountActivity(request.getAmount(), ActivityType.FUNDS_DEPOSIT, account.get());
                        EntityManager em = emf.createEntityManager();
                        em.getTransaction().begin();
                        Account targetAccount = em.find(Account.class, account.get().getId(), LockModeType.PESSIMISTIC_WRITE);
                        try {
                            if (request.getAmount() <= 0.0
                                    || request.getAmount() > environment
                                    .getProperty("deposit.max.amount", Double.class)) {
                                throw new TransactionException(String.format("Invalid transaction amount $ %,.2f. Maximum deposit amount is $ %,.2f",
                                        request.getAmount(), environment.getProperty("deposit.max.amount", Double.class)));
                            } else {

                                Timestamp startDate = new Timestamp(DateTime.now()
                                        .withTimeAtStartOfDay()
                                        .getMillis());
                                Timestamp endDate = new Timestamp(DateTime.now()
                                        .withTimeAtStartOfDay()
                                        .plusDays(1)
                                        .minusSeconds(1)
                                        .getMillis());
                                log.info("Computing account deposit rules between {} and {}...", startDate, endDate);
                                List<AccountActivity> depositActivities = activityRepository
                                        .searchActivity(targetAccount, ActivityType.FUNDS_DEPOSIT,
                                                TransactionStatus.COMPLETED, startDate, endDate);
                                double todaysTotalDeposits = 0;
                                todaysTotalDeposits = depositActivities.stream().map((a) -> a.getAmount()).reduce(todaysTotalDeposits, (accumulator, _item) -> accumulator + _item);
                                if ((depositActivities.size() + 1) > environment.getProperty("deposit.max.daily.frequency",
                                        Integer.class)) {
                                    throw new Exception("Max deposit frequency will be exceeded. Max is "
                                            + environment.getProperty("deposit.max.daily.frequency", Integer.class));
                                } else if (todaysTotalDeposits + request.getAmount() > environment
                                        .getProperty("deposit.max.daily.amount", Double.class)) {
                                    throw new Exception(String.format("Max daily deposit amount "
                                            + "of $ %,.2f will be exceeded. Your total deposits "
                                            + "today is $%,.2f", environment
                                                    .getProperty("deposit.max.daily.amount",
                                                            Double.class), todaysTotalDeposits));
                                } else {
                                    log.info("Deposit rules passed. proceeding..");
                                    targetAccount.setBalance(targetAccount.getBalance() + request.getAmount());

                                    accountActivity.setTransactionStatus(TransactionStatus.COMPLETED);
                                    accountActivity.setAccount(targetAccount);
                                    activityRepository.save(accountActivity);

                                    return accountActivity;
                                }
                            }
                            
                        } catch (Exception e) {
                            accountActivity.setTransactionStatus(TransactionStatus.FAILED);
                            activityRepository.save(accountActivity);
                            throw e;
                        } finally {
                            log.info("Completed. {}", activity);
                            em.merge(targetAccount);
                            em.getTransaction().commit();
                            em.close();
                        }
                    }
                    case FUNDS_WITHDRAWAL: {
                        AccountActivity accountActivity = new AccountActivity(request.getAmount(), ActivityType.FUNDS_WITHDRAWAL, account.get());
                        EntityManager em = emf.createEntityManager();
                        em.getTransaction().begin();
                        Account targetAccount = em.find(Account.class, account.get().getId(), LockModeType.PESSIMISTIC_WRITE);
                        try {
                            if (request.getAmount() <= 0.0
                                    || request.getAmount() > environment
                                    .getProperty("withdrawal.max.amount", Double.class)) {
                                throw new TransactionException(String.format("Invalid transaction amount $ %,.2f. Maximum withdraw amount is $ %,.2f",
                                        request.getAmount(), environment.getProperty("withdrawal.max.amount", Double.class)));
                            } else if (targetAccount.getBalance() - request.getAmount() < 0.0) {
                                throw new TransactionException(String.format("Insufficient account balance. Your current baance is $ %,.2f",
                                        targetAccount.getBalance()));
                            } else {

                                Timestamp startDate = new Timestamp(DateTime.now()
                                        .withTimeAtStartOfDay()
                                        .getMillis());
                                Timestamp endDate = new Timestamp(DateTime.now()
                                        .withTimeAtStartOfDay()
                                        .plusDays(1)
                                        .minusSeconds(1)
                                        .getMillis());
                                log.info("Computing account withdraw rules between {} and {}...", startDate, endDate);
                                List<AccountActivity> withdrawalActivity = activityRepository
                                        .searchActivity(targetAccount, ActivityType.FUNDS_WITHDRAWAL,
                                                TransactionStatus.COMPLETED, startDate, endDate);
                                double todaysTotalWithdrawal = 0;
                                todaysTotalWithdrawal = withdrawalActivity.stream().map((a) -> a.getAmount()).reduce(todaysTotalWithdrawal, (accumulator, _item) -> accumulator + _item);
                                if ((withdrawalActivity.size() + 1) > environment.getProperty("withdrawal.max.daily.frequency",
                                        Integer.class)) {
                                    throw new Exception("Max withdrawal frequency will be exceeded. Max is "
                                            + environment.getProperty("withdrawal.max.daily.frequency", Integer.class));
                                } else if (todaysTotalWithdrawal + request.getAmount() > environment
                                        .getProperty("withdrawal.max.daily.amount", Double.class)) {
                                    throw new Exception(String.format("Max daily withdrawal amount "
                                            + "of $ %,.2f will be exceeded. Your total withdrawal "
                                            + "today is $%,.2f", environment
                                                    .getProperty("withdrawal.max.daily.amount",
                                                            Double.class), todaysTotalWithdrawal));
                                } else {
                                    log.info("Withdrawal rules passed. proceeding..");
                                    targetAccount.setBalance(targetAccount.getBalance() - request.getAmount());

                                    accountActivity.setTransactionStatus(TransactionStatus.COMPLETED);
                                    accountActivity.setAccount(targetAccount);
                                    activityRepository.save(accountActivity);

                                    return accountActivity;
                                }
                            }
                        } catch (Exception e) {
                            accountActivity.setTransactionStatus(TransactionStatus.FAILED);
                            activityRepository.save(accountActivity);
                            throw e;
                        } finally {
                            log.info("Completed. {}", activity);
                            em.merge(targetAccount);
                            em.getTransaction().commit();
                            em.close();
                        }
                    }
                    default: {
                        activity.setTransactionStatus(TransactionStatus.COMPLETED);
                        activityRepository.save(activity);
                        log.info("Copleted. {}", activity);
                        return activity;
                    }
                }
            } else {
                log.warn("Account {} does not exits.", request.getAccountNumber());
                throw new TransactionException("Account does not exist");
            }
        } catch (Exception e) {
            log.error("{}", e.getMessage());
            throw new TransactionException(e.getMessage());
        }
    }

}
