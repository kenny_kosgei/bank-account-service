package co.tala.bank.account.entity;

/**
 * States of a transaction.
 *
 * @author kenny
 */
public enum TransactionStatus {
    CREATED,
    COMPLETED,
    FAILED;
}
