package co.tala.bank.account.entity;

/**
 * Super class for all entities.
 *
 * @author kenny
 * @param <K> Primary key type
 */
public abstract class BaseEntity<K> {

    public abstract K getId();

    public abstract void setId(K id);

}
