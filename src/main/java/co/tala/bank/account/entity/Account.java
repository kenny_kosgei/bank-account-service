package co.tala.bank.account.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * A Bank account record.
 *
 * @author kenny
 */
@Entity
@Table(name = "ACCOUNT")
public class Account extends BaseEntity<Long> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String accountName;
    private String accountNumber;
    private double balance;
    private Timestamp dateCreated;
    @OneToMany(mappedBy = "account")
    private List<AccountActivity> accountActivities;

    public Account() {
    }

    public Account(String accountName, String accountNumber, Timestamp dateCreated) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.dateCreated = dateCreated;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setAccountActivities(List<AccountActivity> accountActivities) {
        this.accountActivities = accountActivities;
    }

    @XmlTransient
    @JsonIgnore
    public List<AccountActivity> getAccountActivities() {
        return accountActivities;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    @PrePersist
    protected void prePersist() {
        dateCreated = new Timestamp(System.currentTimeMillis());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(accountName)
                .append(accountNumber)
                .append(balance)
                .append(dateCreated)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Account)) {
            return false;
        }

        Account other = (Account) obj;

        return new EqualsBuilder()
                .append(id, other.id)
                .append(accountName, other.accountName)
                .append(accountNumber, other.accountNumber)
                .append(balance, other.balance)
                .append(dateCreated, other.dateCreated)
                .isEquals();
    }

    @Override
    public String toString() {
        return String.format("Account[id={},accountName={},accountNumber={},balance={},dateCreated={}]",
                id, accountName, accountNumber, balance, dateCreated);
    }

}
