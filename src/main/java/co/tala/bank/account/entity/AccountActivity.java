package co.tala.bank.account.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Describes various activities happening on a given account.
 *
 * @author kenny
 */
@Entity
@Table(name = "ACCOUNT_ACTIVITY")
public class AccountActivity extends BaseEntity<Long> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private ActivityType activityType = ActivityType.BALANCE_ENQUIRY;
    private double amount;
    private TransactionStatus transactionStatus = TransactionStatus.CREATED;
    private Timestamp dateCreated;
    private Timestamp dateModified;
    @ManyToOne
    @JoinColumn(name = "account")
    private Account account;

    public AccountActivity() {
    }

    public AccountActivity(double amount, ActivityType activityType, Account account) {
        this.amount = amount;
        this.activityType = activityType;
        this.account = account;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    @PreUpdate
    protected void preUpdate() {
        dateModified = new Timestamp(System.currentTimeMillis());
    }

    @PrePersist
    protected void prePersist() {
        dateCreated = new Timestamp(System.currentTimeMillis());
        dateModified = dateCreated;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(activityType)
                .append(amount)
                .append(transactionStatus)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AccountActivity)) {
            return false;
        }
        final AccountActivity other = (AccountActivity) obj;
        return new EqualsBuilder()
                .append(id, other.id)
                .append(activityType, other.activityType)
                .append(amount, other.amount)
                .append(transactionStatus, other.transactionStatus)
                .isEquals();
    }

    @Override
    public String toString() {
        return String.format("AccountActivity[id=%s,activityType=%s,amount=%s,status=%s]", 
                id, activityType,amount,transactionStatus);
    }

}
