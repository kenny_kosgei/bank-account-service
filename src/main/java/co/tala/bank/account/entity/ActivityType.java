package co.tala.bank.account.entity;

/**
 * Types of {@link AccountActivity} performed by user.
 *
 * @author kenny
 */
public enum ActivityType {

    /**
     * Client account balance
     */
    BALANCE_ENQUIRY,

    /**
     * Funds deposit action.
     */
    FUNDS_DEPOSIT,

    /**
     * Funds Withdrawal action.
     */
    FUNDS_WITHDRAWAL;
}
