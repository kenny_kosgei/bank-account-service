package co.tala.bank.account.config;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configurations for Swagger API documentation.
 *
 * @author kenny
 */
@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

    /**
     *
     * @return Builds the configuration for the Bank service API documentation.
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SPRING_WEB)
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("co.tala.bank.account.api"))
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Bank Account Service",
                "Endpoints for checking account balance, depositing and withdrawing cash",
                "API TOS",
                "Terms of service",
                new Contact("Kenny Kosgei", "www.worldbank.com", "kosgei.kenny@gmail.com"),
                "License of API", "worldbank.com/terms-use", Collections.emptyList());
    }
}
