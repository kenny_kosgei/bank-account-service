package co.tala.bank.account.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Boot-strapper for the bank service application.
 *
 * @author kenny
 */
@ComponentScan("co.tala.bank.account.*")
@EnableJpaRepositories("co.tala.bank.account.repository")
@EntityScan("co.tala.bank.account.entity")
@SpringBootApplication
public class Bootstrapper {

    public static void main(String[] args) {
        SpringApplication.run(Bootstrapper.class, args);
    }
}
