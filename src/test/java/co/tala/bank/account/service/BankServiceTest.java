package co.tala.bank.account.service;

import co.tala.bank.account.api.models.AccountRequest;
import co.tala.bank.account.entity.Account;
import co.tala.bank.account.entity.AccountActivity;
import co.tala.bank.account.entity.ActivityType;
import co.tala.bank.account.entity.TransactionStatus;
import co.tala.bank.account.repository.AccountActivityRepository;
import co.tala.bank.account.repository.AccountRepository;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.core.env.Environment;

/**
 *
 * @author kenny
 */
public class BankServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private AccountActivityRepository activityRepository;
    @Mock
    private EntityManagerFactory emf;
    @Mock
    private Environment environment;
    @Mock
    private EntityManager entityManager;
    @Spy
    @InjectMocks
    private BankService service;

    public BankServiceTest() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        //withdrawal rules
        Mockito.when(environment.getProperty(Mockito.eq("withdrawal.max.amount"),
                Mockito.eq(Double.class))).thenReturn(20.0);
        Mockito.when(environment.getProperty(Mockito.eq("withdrawal.max.daily.frequency"),
                Mockito.eq(Integer.class))).thenReturn(2);
        Mockito.when(environment.getProperty(Mockito.eq("withdrawal.max.daily.amount"),
                Mockito.eq(Double.class))).thenReturn(50.0);

        //Deposit rules
        Mockito.when(environment.getProperty(Mockito.eq("deposit.max.amount"),
                Mockito.eq(Double.class))).thenReturn(40.0);
        Mockito.when(environment.getProperty(Mockito.eq("deposit.max.daily.frequency"),
                Mockito.eq(Integer.class))).thenReturn(4);
        Mockito.when(environment.getProperty(Mockito.eq("deposit.max.daily.amount"),
                Mockito.eq(Double.class))).thenReturn(150.0);

        //mock user transaction
        EntityTransaction transaction = Mockito.mock(EntityTransaction.class);
        Mockito.when(emf.createEntityManager()).thenReturn(entityManager);
        Mockito.when(entityManager.getTransaction()).thenReturn(transaction);
    }

    @Test(expected = TransactionException.class)
    public void testTransactAccountBalance_uknown_account() throws TransactionException {
        AccountRequest request = new AccountRequest("UKNOWN#ACCOUNT");
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(Optional.<Account>empty());

        service.transact(request, ActivityType.BALANCE_ENQUIRY);
    }

    @Test
    public void testTransactAccountBalance_ok_account() throws TransactionException {
        AccountRequest request = new AccountRequest("001");
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(Optional.<Account>of(new Account("Dummy Account", "001", new Timestamp(0))));

        AccountActivity activity = service.transact(request, ActivityType.BALANCE_ENQUIRY);

        assertNotNull(activity);
        assertEquals("Unexpected account activity type", ActivityType.BALANCE_ENQUIRY, activity.getActivityType());
        assertEquals("Unexpected account state", 0, activity.getAmount(), 0);
        assertEquals("Unexpected account number", "001", activity.getAccount().getAccountNumber());
        assertEquals("Unexpected account name", "Dummy Account", activity.getAccount().getAccountName());
    }

    @Test(expected = TransactionException.class)
    public void testTransactDeposit_exceedMaxPerTransaction() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 45.5);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(Optional.<Account>of(new Account("Dummy Account", "001", new Timestamp(0))));

        try {
            service.transact(request, ActivityType.FUNDS_DEPOSIT);
        } catch (TransactionException e) {
            assertEquals("Invalid transaction amount $ 45.50. Maximum deposit amount is $ 40.00", e.getMessage());
            throw e;
        }

    }

    @Test(expected = TransactionException.class)
    public void testTransactDeposit_exceedMaxDailyTransactionLimit() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 40.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setId(1L);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        AccountActivity deposiActivity = new AccountActivity(140, ActivityType.FUNDS_DEPOSIT, accountHolder.get());
        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_DEPOSIT),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(Arrays.asList(deposiActivity));

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        try {
            service.transact(request, ActivityType.FUNDS_DEPOSIT);
        } catch (TransactionException e) {
            Mockito.verify(activityRepository).searchActivity(Mockito.eq(accountHolder.get()),
                    Mockito.eq(ActivityType.FUNDS_DEPOSIT),
                    Mockito.eq(TransactionStatus.COMPLETED),
                    Mockito.any(Timestamp.class), Mockito.any(Timestamp.class));
            assertEquals("Max daily deposit amount of $ 150.00 will be exceeded. Your total deposits today is $140.00", e.getMessage());
            throw e;
        }

    }

    @Test(expected = TransactionException.class)
    public void testTransactDeposit_exceedMaxDailyFrequencyLimit() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 40.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setId(1L);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        List<AccountActivity> depositActivities = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            AccountActivity activity = new AccountActivity(10 * (i + 1), ActivityType.FUNDS_DEPOSIT, accountHolder.get());
            activity.setId(i * 1L);
            depositActivities.add(activity);
        }

        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_DEPOSIT),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(depositActivities);

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        try {
            service.transact(request, ActivityType.FUNDS_DEPOSIT);
        } catch (TransactionException e) {
            Mockito.verify(activityRepository).searchActivity(Mockito.eq(accountHolder.get()),
                    Mockito.eq(ActivityType.FUNDS_DEPOSIT),
                    Mockito.eq(TransactionStatus.COMPLETED),
                    Mockito.any(Timestamp.class), Mockito.any(Timestamp.class));
            assertEquals("Max deposit frequency will be exceeded. Max is 4", e.getMessage());
            throw e;
        }

    }

    @Test
    public void testTransactDeposit_OK() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 10.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setId(1L);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_DEPOSIT),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(Collections.EMPTY_LIST);

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        AccountActivity activity = service.transact(request, ActivityType.FUNDS_DEPOSIT);
        Mockito.verify(activityRepository).searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_DEPOSIT),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class), Mockito.any(Timestamp.class));

        assertEquals("Unexpected account activity type", ActivityType.FUNDS_DEPOSIT, activity.getActivityType());
        assertEquals("Unexpected account state", 10, activity.getAmount(), 0);
        assertEquals("Unexpected account number", "001", activity.getAccount().getAccountNumber());
        assertEquals("Unexpected account name", "Dummy Account", activity.getAccount().getAccountName());

    }

    @Test(expected = TransactionException.class)
    public void testTransactWithdraw_exceedMaxPerTransaction() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 30);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(Optional.<Account>of(new Account("Dummy Account", "001", new Timestamp(0))));

        try {
            service.transact(request, ActivityType.FUNDS_WITHDRAWAL);
        } catch (TransactionException e) {
            assertEquals("Invalid transaction amount $ 30.00. Maximum withdraw amount is $ 20.00", e.getMessage());
            throw e;
        }

    }

    @Test(expected = TransactionException.class)
    public void testTransactWithdraw_InsufficientBalance() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 10.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setId(1L);
        account.setBalance(5);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        AccountActivity withdrawActivities = new AccountActivity(45, ActivityType.FUNDS_WITHDRAWAL, accountHolder.get());
        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(Arrays.asList(withdrawActivities));

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        try {
            service.transact(request, ActivityType.FUNDS_WITHDRAWAL);
        } catch (TransactionException e) {
            assertEquals("Insufficient account balance. Your current baance is $ 5.00", e.getMessage());
            throw e;
        }

    }

    @Test(expected = TransactionException.class)
    public void testTransactWithdraw_exceedMaxDailyTransactionLimit() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 10.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setId(1L);
        account.setBalance(120);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        AccountActivity withdrawActivities = new AccountActivity(45, ActivityType.FUNDS_WITHDRAWAL, accountHolder.get());
        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(Arrays.asList(withdrawActivities));

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        try {
            service.transact(request, ActivityType.FUNDS_WITHDRAWAL);
        } catch (TransactionException e) {
            Mockito.verify(activityRepository).searchActivity(Mockito.eq(accountHolder.get()),
                    Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                    Mockito.eq(TransactionStatus.COMPLETED),
                    Mockito.any(Timestamp.class), Mockito.any(Timestamp.class));
            assertEquals("Max daily withdrawal amount of $ 50.00 will be exceeded. Your total withdrawal today is $45.00", e.getMessage());
            throw e;
        }

    }

    @Test(expected = TransactionException.class)
    public void testTransactWithdrawal_exceedMaxDailyFrequencyLimit() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 15.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setId(1L);
        account.setBalance(205.55);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        List<AccountActivity> withdrawActivities = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            AccountActivity activity = new AccountActivity(5, ActivityType.FUNDS_WITHDRAWAL, accountHolder.get());
            activity.setId(i * 1L);
            withdrawActivities.add(activity);
        }

        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(withdrawActivities);

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        try {
            service.transact(request, ActivityType.FUNDS_WITHDRAWAL);
        } catch (TransactionException e) {
            Mockito.verify(activityRepository).searchActivity(Mockito.eq(accountHolder.get()),
                    Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                    Mockito.eq(TransactionStatus.COMPLETED),
                    Mockito.any(Timestamp.class), Mockito.any(Timestamp.class));
            assertEquals("Max withdrawal frequency will be exceeded. Max is 2", e.getMessage());
            throw e;
        }

    }

    @Test
    public void testTransactWithdraw_OK() throws TransactionException {
        AccountRequest request = new AccountRequest("001", 16.00);
        Account account = new Account("Dummy Account", "001", new Timestamp(0));
        account.setBalance(500.00);
        account.setId(1L);
        Optional<Account> accountHolder = Optional.<Account>of(account);
        Mockito.when(accountRepository.findDistinctByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(accountHolder);
        Mockito.when(activityRepository.searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class),
                Mockito.any(Timestamp.class))).thenReturn(Collections.EMPTY_LIST);

        Mockito.when(entityManager.find(Mockito.eq(Account.class), Mockito.eq(1L),
                Mockito.eq(LockModeType.PESSIMISTIC_WRITE))).thenReturn(account);

        AccountActivity activity = service.transact(request, ActivityType.FUNDS_WITHDRAWAL);
        Mockito.verify(activityRepository).searchActivity(Mockito.eq(accountHolder.get()),
                Mockito.eq(ActivityType.FUNDS_WITHDRAWAL),
                Mockito.eq(TransactionStatus.COMPLETED),
                Mockito.any(Timestamp.class), Mockito.any(Timestamp.class));

        assertEquals("Unexpected account activity type", ActivityType.FUNDS_WITHDRAWAL, activity.getActivityType());
        assertEquals("Unexpected account state", 16, activity.getAmount(), 0);
        assertEquals("Unexpected account number", "001", activity.getAccount().getAccountNumber());
        assertEquals("Unexpected account name", "Dummy Account", activity.getAccount().getAccountName());

    }

}
